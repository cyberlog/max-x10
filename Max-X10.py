import discord
import json
import os
import requests
from discord.ext import commands
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials




#get the token
with open('config.json', 'r') as f:
    config = json.load(f)

token = config['token']

#prefix
client = commands.Bot(command_prefix = '~')

@client.command()
async def load(ctx, extension):
    client.load_extension(f'cogs.{extension}')


for filename in os.listdir('./cogs'):
    if filename.endswith('.py'):
        client.load_extension(f'cogs.{filename[:-3]}')


client.run(token)