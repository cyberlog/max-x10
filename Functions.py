

#gets users input?
#
junction = input('which function do you want to test? \n -enumerate \n -map \n -lambda \n -filter \n -constructors \n')


#Enumerate

#enumerate: automatically labels every iteration
if (junction == 'enumerate'):
    for i, num in enumerate(range(10)):
        print(i, 'I automatically number my items')


#Map

#modifies entire lists using an user defined function function
elif (junction == 'map'):
    def addition(n): 
        return n + 23 
  
    # We double all numbers using map() 
    numbers = (1, 2, 3, 4) 

    #map(function, list)
    result = map(addition, numbers) 
    print(list(result)) 


#Lambda

# a short hand for a function 
elif (junction == 'lambda'):
    function = lambda argument1, argument2 : argument1 + argument2 * 10
    print(function(3, 8)) 


#Filter

#filters out items in a list based off of a function
elif (junction == 'filter'):
    number_list = range(-5, 5)
    #less_than_zero = list(filter(lambda x: x < 0, number_list))
    def fun(variable): 
        letters = ['a', 'e', 'i', 'o', 'u', 'r'] 
        if (variable in letters): 
            return True
        else: 
            return False
  
  
    # sequence 
    sequence = ['g', 'e', 'e', 'j', 'k', 's', 'p', 'r'] 
  
    # using filter function 
    filtered = filter(fun, sequence) 
  
    print('The filtered letters are:') 
    for s in filtered: 
        print(s) 

elif (junction == 'constructors'):
    class GeekforGeeks: 
    
        # default constructor 
        def __init__(self): 
            self.geek = "GeekforGeeks"
    
        # a method for printing data members 
        def print_Geek(self): 
            print(self.geek) 
    
    
    # creating object of the class 
    obj = GeekforGeeks() 
    
    # calling the instance method using the object obj 
    obj.print_Geek() 
